# Overview
This is an eleventy project that is used to generate a static site that explains tips and tricks about the Kingdom Hearts 2 Final Mix Randomizer.  The site lists locations of chests, describes other checks that could be important, and offers a few tricks for getting around some of the more difficult situations.  The site is currently in early phases and will be updated to reflect that.  **The site is currently not live but should be soon.**


# KH2FM Randomizer
The KH2FM Randomizer was created by Valaxor and can be found [here](https://randomizer.valaxor.com/#/seed).  That website will generate a .pnach file that can be used with the Garden of Assemblage Mod in the PCSX2 emulator.

# Garden of Assemblage Mod
This mod was created by sonicshadowsilver2 and more info about it can be found [here](https://docs.google.com/document/d/1GYjEnrM_TIk7qyO75clPLYD-_nP5wTR7K6SE-Wn-QCg/edit).  The mod essentially creates a new hub-world that allows Sora to access the different worlds in the game in any order and without use of the Gummi Ship.

# Randomizer Installation Help
There are several guides that will help you get started with the randomizer.  I recommend taking a look at the ones listed at the [Discord](https://discord.gg/GcJR7Fv).  Some parts of the process change often, so it's best to follow one of the maintained guides listed there.